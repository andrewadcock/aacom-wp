<header class="banner">
    <div class="container">
        <div class="burger">
            <a href="#"><span class="sr-only">MENU</span></a>
        </div>
        <div class="logo-name">
            <div class="logo">
                <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img
                        src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bear-side.png"
                        alt="<?php bloginfo('name'); ?>"/></a>
            </div>
            <div class="name">
                <a class="brand-name" href="<?= esc_url(home_url('/')); ?>">Andrew Adcock</a>
            </div>
        </div>
        <div class="social">
            <ul>
                <li class="twitter">
                    <a href="https://twitter.com/theandrewadcock" target="_blank"><span
                            class="sr-only">Twitter</span></a>
                </li>
                <li class="github">
                    <a href="https://github.com/andrewadcock" target="_blank"><span class="sr-only">GitHub</span></a>
                </li>
                <li class="wordpress">
                    <a href="https://wordpress.org/support/profile/andrewadcock" target="_blank"><span class="sr-only">WordPress</span></a>
                </li>
                <li class="linkedin">
                    <a href="https://www.linkedin.com/in/andrew-adcock-ab818613" target="" _blank"><span
                        class="sr-only">LinkedIn</span></a>
                </li>
            </ul>
        </div>
        <nav class="nav-primary">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
        </nav>
        <div class="clearfix"></div>
    </div>
</header>
