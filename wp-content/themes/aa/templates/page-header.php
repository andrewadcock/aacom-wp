<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  <?php if(! is_home()) { ?><h1><?= Titles\title(); ?></h1> <?php } ?>
</div>
