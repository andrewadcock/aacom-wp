<footer class="content-info">
    <div class="container">
        <div class="social">
            <ul>
                <li class="twitter">
                    <a href="https://twitter.com/theandrewadcock" target="_blank"><span
                            class="sr-only">Twitter</span></a>
                </li>
                <li class="github">
                    <a href="https://github.com/andrewadcock" target="_blank"><span class="sr-only">GitHub</span></a>
                </li>
                <li class="wordpress">
                    <a href="https://wordpress.org/support/profile/andrewadcock" target="_blank"><span class="sr-only">WordPress</span></a>
                </li>
                <li class="linkedin">
                    <a href="https://www.linkedin.com/in/andrew-adcock-ab818613" target="" _blank"><span
                        class="sr-only">LinkedIn</span></a>
                </li>
            </ul>
        </div>
        <nav class="nav-primary">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
        </nav>
        <div class="clearfix"></div>
        <div class="copyright">
            &copy; <?php the_date('Y'); ?> <a href="http://andrewadcock.com">Andrew Adcock</a>. Built with <a href="https://github.com/smutek/BourbonySage" target="_blank">Bourbony Sage</a>.
        </div>
    </div>
</footer>
