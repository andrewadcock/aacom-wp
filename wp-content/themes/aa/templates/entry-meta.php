<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
<div class="categories">
    <?php
    $cats = wp_get_post_categories(get_the_ID());
    if( count($cats) == 1 ) {
     echo "Category: ";
    } else {
        echo "Categories: ";
    }
    $ccount = 0;
    foreach ($cats as $c) {
        $cat = get_category($c);
        if( $ccount != 0) echo ', ';
        echo "<a href='/category/" . $cat->slug . "'>" . $cat->name . "</a>";
        $ccount++;
    }
    ?>
</div>
<div class="tags">Tags:
    <?php
    $tags = wp_get_post_tags($post->ID);
    $tcount = 0;
    foreach ($tags as $t) {
        $tag = get_category($t);
        if( $tcount != 0) echo ', ';
        echo "<a href='/tag/" . $tag->slug . "'>" . $tag->name . "</a>";
        $tcount++;
    }
    ?>
</div>